<?php get_header(); ?> <div class="slide bg-home pb-5"><div class="container h-100 pb-5"><div class="d-flex h-100 align-items-end"><div class="box text-white w-100"><div class="box-text"><div id="carouselExampleControls" class="carousel slide w-100"><div class="carousel-inner"><div class="carousel-item active h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">A agenda dos executivos está sobrecarregada?</div><b>Estratégia Societária</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">A empresa está tomando decisões de Alto Risco?</div><b>Diagnóstico Econômico Financeiro</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">A gestão do Fluxo de Caixa é um problema?</div><b>Monitoramento do Capital Circulante</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">Existe pressão por parte dos credores?</div><b>Recuperação Judicial e Renegociação do Passivo</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">Os custos fixos ou variáveis podem ser ainda mais reduzidos?</div><b>Melhoria Operacional</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">A agenda dos executivos está sobrecarregada?</div><b>Gestão Societária</b></a></div></div><div class="carousel-item h-100"><div class="d-flex align-items-end justify-content-start h-100"><a href="servicos"><div class="font-weight-light">A gestão é tipicamente familiar?</div><b>Estratégia Societária</b></a></div></div></div><a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a><a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span></a></div></div></div></div></div></div> <?php include('sobre-nos.php') ?> <div class="my-5 bg-green py-5 siegen-aj"><div class="container"><div class="row justify-content-md-end text-white align-items-center"><div class="col-12 text-center mb-4"><h2>Como a Siegen pode<br>ajudar a sua empresa?</h2></div><div class="col-md-5"><div class="d-flex align-items-center mt-4 item wow bounceInRight"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group59.png" alt="Aperto de mão simbolizando renegociação"><h3>Monitoramento do<br>Capital Circulante</h3></div><div class="d-flex align-items-center mt-4 item wow bounceInRight"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group60.png" alt="Aperto de mão simbolizando renegociação"><h3>Melhoria<br>Operacional</h3></div><div class="d-flex align-items-center mt-4 item wow bounceInRight"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone2.png" alt="Aperto de mão simbolizando renegociação"><h3>Apoio ao M&A<br>(Fusão e Aquisição)</h3></div><div class="d-flex align-items-center mt-4 item wow bounceInRight"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group61.png" alt="Aperto de mão simbolizando renegociação"><h3>Diagnóstico<br>Econômico-Financeiro</h3></div></div><div class="col-md-5"><div class="d-flex align-items-center mt-4 item wow bounceInLeft"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group58.png" alt="Aperto de mão simbolizando renegociação"><h3>Recuperação<br>Judicial</h3></div><div class="d-flex align-items-center mt-4 item wow bounceInRight"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone1.png" alt="Aperto de mão simbolizando renegociação"><h3>Shepherd Dog</h3></div><div class="d-flex align-items-center item wow bounceInLeft"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group56.png" alt="Aperto de mão simbolizando renegociação"><h3>Renegociação<br>de Passivo</h3></div><div class="d-flex align-items-center mt-4 item wow bounceInLeft"><img class="mr-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group57.png" alt="Aperto de mão simbolizando renegociação"><h3>Estratégia<br>Societária</h3></div></div></div><div class="col-12 text-center mt-5 mb-3"><a class="color-green hover-bt px-5 py-3 bg-white" href="servicos">Saiba mais</a></div></div></div><div class="container depoimentos"><div class="d-md-flex flex-wrap"><div class="col-12 text-center text-center position-relative"><h2 class="color-green mb-3">O que falam da Siegen</h2><p class="mb-0 mt-md-3">Veja alguns depoimentos de parceiros e clientes que confiam no nosso trabalho.</p></div><div class="col-md-12"><img class="quota" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quote.png"><div class="owl-depoimentos owl-carousel owl-theme"> <?php

        $paged = get_query_var('paged') ? get_query_var('paged') : 1;

        $args = array(

          'post_type' => 'depoimentos',

          'order' => 'ASC',

          'posts_per_page' => '-1',

          'paged' => $paged,

        );

        $loop = new wp_query($args);

        while ($loop->have_posts()) : $loop->the_post() ?> <div class="item"><p class="depo-txt col-11 col-md-12 ml-auto"> <?= the_field('descricao') ?> </p><div class="d-flex"><div class="col-md-10"><div class="d-flex align-items-center mt-2"><div class="mr-2"><img src="<?= the_field('logo_da_empresa') ?>" title="Dedini - Fornecedo de equipamentos industriais" alt="Dedini - Fornecedo de equipamentos industriais"></div><div><b> <?= the_field('nome_da_pessoa') ?> </b><br> <?= the_field('cargo') ?> </div></div></div></div></div> <?php endwhile; ?> </div></div><div class="text-center w-100"><div id="customNavs"></div></div></div></div><div class="my-4"> <?php include('nossos-numeros.php'); ?> </div><div class="my-md-2"><div class="container"><div class="row"><h2 class="col-12 text-center mb-2 color-green">Siegen na Mídia</h2><!-- <div class="col-md-7">

        <div class="d-flex text-center flex-wrap align-items-center">

          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/infomoney.png" alt="Infomoney">

          </div>

          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/veja.png" alt="Veja">

          </div>

          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/diario-comercio.png" alt="Diário do comércio">

          </div>


          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/jovempan.png" alt="Diário do comércio">

          </div>


          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cultura.png" alt="Cultura">

          </div>

          <div class="col-md-4 my-3">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/istoe.png" alt="Isto é dinheiro">

          </div>

        </div>

      </div> --><div class="col-md-6 text-center m-auto"><p class="mb-5">A credibilidade da Siegen refletida em artigos e entrevistas cedidas pelos nossos profissionais aos grandes veículos de imprensa do Brasil</p></div></div></div><div class="videos"><div class="px-4"><div class="text-center m-0"><iframe class="video-home" width="560" height="315" src="https://www.youtube.com/embed/MEkfeMb4vqI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div><p class="mb-3 text-center">Para mais vídeos, visite e se inscreva em nosso canal do YouTube</p><!-- <div class="text-center">
        <div id="customNav" class="cnavs"></div>
        <div class="d-md-none">
          <div class="my-4 owl-dots" id="navDots"></div>
        </div>
        <div class="nav-m owl-nav"></div> 
      </div> --></div></div><div class="col-12 d-flex justify-content-center py-3"><a href="siegen-na-midia" class="bg-green hover-bt px-5 py-3 text-white">Veja nossas matérias</a></div><!-- <div class="mt-md-5 mb-4"> <?php include('equipe.php') ?> </div> --><div class="clientes pb-0 py-md-3 mb-4"><div class="container"><div class="text-center m-auto"><h2 class="mb-4 color-green">Clientes e Ex-Clientes</h2><p class="mb-md-5 mb-4">Temos orgulho em apoiar grandes e médias empresas de diversos segmentos da economia nacional.</p></div><!-- <div class="owl-clientes owl-carousel owl-theme"> <?php

        $index = 0;

        // check if the repeater field has rows of data
        if (have_rows('logo', 7)) :

          // loop through the rows of data
          while (have_rows('logo', 7)) : the_row();

            $index++;

            if ($index  == 1) {

              echo '<div class="item d-flex flex-wrap text-white justify-content-center">';
            }

            // display a sub field value


        ?> <img src="<?= the_sub_field('img', 7); ?>"> <?php

            if ($index == 15) {

              $index = 0;

              echo '</div>';
            }

          endwhile;

        else :

        // no rows found

        endif;

        ?> </div>
      <div class="text-center">
        <div class="cnavs pt-2" id="clientesNav"></div>
      </div> --></div></div></div> <?php include('faca-parte.php') ?> <?php get_footer(); ?>