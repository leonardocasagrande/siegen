$(".owl-sobre").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: false,
  nav: false,
  responsive: {
    0: {
      items: 1
    }
  }
});

$(".owl-materias").owlCarousel({
  loop: false,
  margin: 10,
  dots: true,
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: false,
  nav: true,
  navContainer: "#materiaNav",
  dotsContainer: "#materiaDots",
  navText: [
    "<i class='fas fa-arrow-left next color-grey'></i>",
    "<i class='fas color-grey fa-arrow-right ml-3'></i>"
  ],
  responsive: {
    0: {
      items: 1
    },
    1200: {
      items: 3
    }
  }
});

$(".owl-clientes").owlCarousel({
  loop: true,
  margin: 10,
  autoplay: true,
  autoplayTimeout: 6000,
  autoplayHoverPause: false,
  dots: true,
  responsive: {
    0: {
      items: 1
    }
  }
});

if ($(window).width() >= 1200) {
  $(".nav-d").append("<div id='customNav'> </div>");
} else {
  $(".nav-m").append("<div id='customNav' class='text-center'> </div>");

  $(".add-carousel").addClass("owl-gestores owl-carousel owl-theme ");
}

$(".owl-gestores").owlCarousel({
  loop: true,
  margin: 10,
  dots: false,
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: false,
  nav: true,
  navText: [
    "<i class='fas fa-arrow-left next color-grey'></i>",
    "<i class='fas color-grey fa-arrow-right ml-3'></i>"
  ],
  responsive: {
    0: {
      items: 1
    }
  }
});

$(".owl-videos").owlCarousel({
  loop: true,
  margin: 80,
  nav: true,
  center: true,
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: false,
  dots: true,
  navContainer: "#customNav",
  dotsContainer: "#navDots",
  navText: [
    "<i class='fas fa-arrow-left next color-grey'></i>",
    "<i class='fas color-grey fa-arrow-right ml-3'></i>"
  ],

  responsive: {
    0: {
      items: 1
    },

    600: {
      items: 2
    },
    1200: {
      items: 3
    }
  }
});

$(".owl-depoimentos").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  dots: true,
  autoplay: true,
  autoplayTimeout: 5000,
  autoplayHoverPause: false,
  navText: [
    "<i class='fas fa-arrow-left next color-grey'></i>",
    "<i class='fas color-grey fa-arrow-right ml-3'></i>"
  ],
  navContainer: "#customNavs",
  dotsContainer: "#customDots",
  responsive: {
    0: {
      items: 1
    }
  }
});

$(".owl-dots").wrap(
  "<div class='d-md-flex navv justify-content-center  align-items-center'>   </div>"
);

// $(".owl-sobre .navv").append(
//   "<br> <a href='sobre-nos' class='bg-green px-5 py-3 mt-4 hover-bt text-white'>Saiba mais</a>  "
// );

new WOW().init();

// $(".wpcf7-validates-as-tel")
//   .mask("(99) 99999-9999")
//   .focusout(function (event) {
//     var target, phone, element;
//     target = event.currentTarget ? event.currentTarget : event.srcElement;
//     phone = target.value.replace(/\D/g, "");
//     element = $(target);
//     element.unmask();
//     if (phone.length > 10) {
//       element.mask("(99) 99999-99999");
//     } else {
//       element.mask("(99) 9999-99999");
//     }
//   });

$(".alphaonly").bind("keyup blur", function () {
  var node = $(this);
  node.val(node.val().replace(/[^a-z]/g, ""));
});

$(".carousel").carousel({
  interval: 4000
});
