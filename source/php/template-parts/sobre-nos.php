<div class="container my-5 text-center text-md-left sobre-nos">

  <div class="row">

    <div class="col-md-5">

      <h2 class="color-green mb-4">Sobre nós</h2>

      <p>Fundada em 1996 por Augusto Paes Barreto e Fábio Astrauskas, a Siegen tornou-se rapidamente uma das mais conceituadas consultorias em gestão estratégica e recuperação de empresas do Brasil. Os mais de 300 projetos desenvolvidos em nossos <b>mais de 20 anos</b> de história, nos trouxeram a <b>credibilidade</b>, refletida nas parcerias com mais de 60 instituições financeiras, onde auxiliamos na <b>captação de linhas de crédito a curto prazo para os nossos clientes.</b>
      </p>

      <img class="d-none d-md-block my-3 pb-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobre-nos-foto.png" alt="Sala de reunião Siegen" title="Sala de reunião Siegen">

      <p>
      Hoje nosso escritório conta com mais de 80 colaboradores altamente capacitados, especializados em <b>áreas multidisciplinares</b>, que atuam na busca das melhores soluções para sua empresa.
      </p>

    </div>

    <div class="col-md-7">

      <div class="owl-sobre owl-carousel owl-theme mb-md-4">

        <div class="item">


          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/IMG_0004.jpg" alt="Sala de espera Siegen">


        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/MaskGroup.png" alt="Sala de espera Siegen">


        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/IMG_0042.jpg" alt="Sala de espera Siegen">

        </div>

      </div>

      <div class="col-12 my-md-3 d-flex justify-content-center my-4">
        <a href="sobre-nos" class="bg-green px-5 py-3 hover-bt text-white">Saiba mais</a>
      </div>

    </div>



  </div>

</div>