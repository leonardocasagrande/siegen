<div class="bg-light d-none d-md-block py-md-1">

  <div class="container py-3 text-center mb-4">

    <div class="col-md-12 mb-2">

      <h2 class="text-center color-green">Nossos Números</h2>

    </div>

    <img class="my-3 pb-3 pb-md-4 mxw-900" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/nosso-n.png" title="Diagrama representado números da Siegen" alt="Diagrama representado números da Siegen ">

    <!-- <div class="col-12">

      <a class="color-green text-center border-green px-5 py-3 hover-bt" href="sobre-nos">Saiba mais</a>

    </div> -->

  </div>

</div>

<div class="bg-green d-md-none  nossos-numeros py-4">

  <div class="container">

    <div class="d-md-flex flex-wrap justify-content-around">

      <div class="col-md-3">

        <h2 class="text-center text-md-left text-white mt-md-5">Nossos <br> Números</h2>

      </div>

      <div class="col-md-9 d-flex flex-wrap full-center">

        <div class="item">

          <div class="box">

            <span>+30</span>

            <p>Projetos <br>
              Realizados</p>

          </div>

        </div>

        <div class="item">

          <div class="box">

            <span>+40</span>

            <p>Quantidade Atual <br>
              de Clientes</p>

          </div>

        </div>

        <div class="item">

          <div class="box">

            <span>+60</span>

            <p> Parceiros <br>
              Financeiros</p>


          </div>

        </div>

        <div class="item">

          <div class="box">

            <span>+80</span>

            <p> Executivos <br> Capacitados</p>


          </div>

        </div>

        <div class="item">

          <div class="box">

            <span class="text-capitalize">+R$1,5bi</span>

            <p>Volume em <br> Operação (VOP)</p>

          </div>

        </div>

        <div class="item">

          <a class="color-green px-5 py-3 bg-white hover-bt" href="sobre-nos">Saiba mais</a>


        </div>

      </div>

    </div>


  </div>

</div>