<div class="bg-dgrey py-5 my-md-4">

  <div class="container">

    <div class="row align-items-center justify-content-center">

      <div class="col-lg-5 col-md-6 text-white mb-4 mb-md-0">

        <h3>Faça parte da equipe!</h3>

        <p>Você se identifica com a nossa missão no mercado e com os valores que propagamos? Envie seu currículo para o nosso banco de dados.</p>

      </div>

      <div class="col-lg-3 col-md-4">

        <a target="_blank" href="https://jobs.kenoby.com/siegen/" class="bg-green hover-bt px-5 py-3 text-white">Saiba mais</a>

      </div>

    </div>

  </div>

</div>