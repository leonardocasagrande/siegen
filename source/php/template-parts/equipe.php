<div class="equipe py-3 full-center">

  <div class="container">

    <div class="col-12 text-center text-white">

      <div class="box">

        <h2 class="mb-4">Faça parte da equipe!</h2>

        <p class="mb-5">O total comprometimento dos membros da equipe é o que garante a entrega de todas as etapas dos projetos em que nos envolvemos.</p>

      </div>

      <a class="color-green px-5 py-3 bg-white hover-bt" href="equipe">Ver equipe</a>

    </div>

  </div>

</div>