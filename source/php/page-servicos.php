<?php get_header(); ?>

<section class="bg-servicos slide">
  <div class="container">
    <div class="row align-items-end height-format">
      <div class="col-md-7 text-white">
        <h1 class="pb-md-3">Serviços</h1>
        <p>Experiência e Disciplina produzem resultados. A Siegen tem a solução ideal para resolver os problemas da sua empresa.
        </p>
      </div>
    </div>
  </div>
</section>

<section class="bg-dgrey section-help">
  <div class="container">
    <h1 class="text-white text-center py-4">Como a Siegen pode <br> ajudar a sua empresa?</h1>
    <div class="row py-4">
      
      <div class="col-md-6">

        <div class="col-md-10 ml-auto d-flex mb-3">

        <div class="col-3 pl-md-0 align-items-center">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group59.png" alt="">
        </div>
        <!-- Mobile 
        <div class="col-3 py-2 d-md-none d-sm-block">
          <hr class="hr-format m-0">
        </div>
          Fim Mobile -->
        <div class="col-8 align-items-center d-flex">
          <h3 class="span-format">Monitoramento do <br> Capital Circulante</h3>
        </div>
        </div>

        <div class="col-10 ml-auto">
        <p class="text-white">Por meio de ferramentas como o fluxo de caixa projeto 13 semanas, a Siegen consegue diagnosticar desacertos nos meios circulantes da empresa. Além disso, pela nossa <b>credibilidade no mercado financeiro, captamos de recursos pelos mais de 60 instituições financeiras parceiras</b>, intermediando o contato diário das modalidades disponíveis de crédito: antecipação de recebíveis, comissárias, fomentos, CCB e contas garantidas, segundo os parâmetros de juros acordados previamente com o cliente.</p>
        </div>
      </div>


      <div class="col-md-6">
        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group58.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
           Fim Mobile  -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Recuperação<br>Judicial</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">
            Não perca o sono e nem se desvie do seu foco principal de atuação. Realizamos para você as análises dos processos de recuperação judicial com o intuito de elaborar os melhores planos de recuperação e, além disso, atuamos diretamente na negociação com seus credores e no acompanhamento das assembleias.
          </p>
        </div>
      </div>

      


      <div class="col-md-6">

        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group60.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
           Fim Mobile -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Melhoria Operacional</h3>
          </div>
        </div>
        <?php
        $txt_melhoria = "Aumente a sua competitividade com uma ampla análise das áreas operacionais (administrativo, financeiro, controladoria, vendas, marketing e RH). Nossa equipe realiza uma avaliação dos processos internos e atua, quando necessário, na readequação do trabalho visando sempre o sucesso da sua empresa. Focamos na interpretação de valiosas informações, da própria empresa, que servirão de base para a tomada de decisões estratégicas. Nosso corpo consultivo possui competências e habilidades para atuar nas diversas áreas da organização, readequando-as para torná-la mais competitiva e robusta no cenário de atuação."
        ?>

        <div class="col-10 ml-auto">
          <p class="text-white"><?= $txt_melhoria ?></p>
        </div>
      </div>


      <div class="col-md-6">

        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone1.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
          Fim Mobile -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Shepherd Dog</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">
            O <i>Shepherd dog</i> é o monitoramento econômico-financeiro realizado por um agente independente. A Siegen, por meio de sua reconhecida e extensa experiência em reestruturação de empresas, visa não somente trazer segurança aos investidores/credores, mas também auxiliar e aconselhar a empresa na sua gestão.</p>
        </div>
      </div>


      <div class="col-md-6">

        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone2.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
          Fim Mobile -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format"> Apoio ao M&A<br>(Fusão e Aquisição)</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">
          Nós realizamos o cálculo do valor de sua empresa (Valuation) e fazemos o Due Diligence, que é um procedimento de estudo e investigação de diferentes fatores de uma empresa, tendo como objetivo, analisar possíveis riscos que ela possa trazer para os diferentes públicos interessados. Nós também recomendamos as medidas necessárias pós-fusões/joint venture.</p>
        </div>
      </div>


      

      <!-- Teste -->

      

      


      <div class="col-md-6">
        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group56.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>-->

          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Renegociação <br> de Passivo</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">Restabelecer a saúde financeira é o que permitirá a continuidade da operação da sua empresa e trará a capacidade de investimento em novas oportunidades de negócios. Realizamos o diagnóstico dos passivos da empresa junto a fornecedores, instituições financeiras e de fomento mercantil (factorings e FIDC), pessoal e prestadores de serviços. O alongamento do passivo, deságios, acordos ou adequações para pagamento junto ao fluxo de caixa, visam sempre a recuperação econômico-financeiro da empresa.</p>

          <p class="text-white"> Preocupe-se apenas com as suas atividades e deixe que nós cuidamos das suas renegociações.</p>
        </div>
      </div>


      


      <div class="col-md-6">
        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group61.png" alt="">
          </div>
          <!-- Mobile
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
           Fim Mobile   -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Diagnóstico <br>Econômico-Financeiro</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">

            Temos todas as ferramentas necessárias para realizar um completo diagnóstico da situação econômico-financeira da sua empresa. O objetivo é elaborar planos orçamentários para investimentos e avaliar a viabilidade econômica de fusões ou aquisições.

          </p>
        </div>
      </div>

      <div class="col-md-6">

        <div class="col-md-10 ml-auto d-flex mb-3">

          <div class="col-3 pl-md-0 align-items-center">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Group57.png" alt="">
          </div>
          <!-- Mobile 
          <div class="col-3 py-2 d-md-none d-sm-block">
            <hr class="hr-format m-0">
          </div>
          Fim Mobile -->
          <div class="col-8 align-items-center d-flex">
            <h3 class="span-format">Estratégia <br>Societária</h3>
          </div>
        </div>

        <div class="col-10 ml-auto">
          <p class="text-white">
            Uma empresa só se mantém sólida quando suas bases estão bem estruturadas e, muitas vezes, um olhar externo permite que se encontrem pontos de melhoria. Seja na mediação de conflitos, em processos de sucessão ou iniciativas como uma mentoria de coaching, temos a expertise para identificar a sua necessidade e atuar da forma mais assertiva.</p>
        </div>
      </div>

      <div class="col-md-12 d-flex justify-content-center py-3 mt-5">
        <a href="<?= get_site_url(); ?>/contato" class="btn hover-bt bg-green text-white px-4 py-2">Entre em contato</a>
      </div>

    </div>
  </div>
</section>


<section>
  <div class="container py-5">
    <div class="col-md-10 m-auto">
      <h1 class="text-center py-3">Processo de Recuperação</h1>

      <div class=" recuperacao d-none flex-wrap  mt-4 d-xl-flex">

        <div class="col-md-6 pr-0">

          <div class="text-center">

            <h3>Recuperação</h3>

            <div class="d-flex align-items-center">

              <div class="box">

                <div class="bol mr-4 d-flex flex-wrap justify-content-center align-items-center">

                  <div class="border-sec">

                    Passivo

                  </div>

                </div>

                <div class="color-green mt-4 pr-1 border-r w-200">

                  Passado

                </div>

              </div>

              <div class="box">

                <div class="bol mr-4 d-flex flex-wrap justify-content-center align-items-center">

                  <div class="border-sec">

                    Capital <br> Circulante

                  </div>

                </div>

                <div class="color-green mt-4 mr-4 border-r w-200">

                  Presente

                </div>

              </div>

            </div>

          </div>

        </div>

        <div class="col-md-6 pl-0">

          <div class="text-center">

            <h3>Crescimento</h3>

            <div class="d-flex align-items-center flex-wrap">

              <div class="box">

                <div class="bol mr-4 d-flex justify-content-center align-items-center">

                  <div class="border-sec">

                    Operacional

                  </div>

                </div>

              </div>

              <div class="box">

                <div class="bol mr-4 d-flex justify-content-center align-items-center">

                  <div class="border-sec">

                    Societária

                  </div>

                </div>

              </div>

              <div class="color-green mt-4 col-12">

                Futuro

              </div>

            </div>

          </div>

        </div>

        <div class="w-100">

          <div class="row align-items-center justify-content-center">

            <div class="detail-green bg-green"></div> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow.png">

          </div>

        </div>

      </div>

      <div class="col-12 d-flex justify-content-center d-xl-none py-4">
        <img class="img-fluid " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/processo-mobile.png" alt="">
      </div>
    </div>
  </div>
</section>

<section class="section-works">

  <div class="container">

    <div class="d-flex flex-wrap py-5">

      <div class="col-md-6">

        <div class="col-md-5">
          <h1 class="text-md-left text-center py-3">Como funciona?</h1>
        </div>

      </div>

      <div class="col-md-6 position-relative py-4 py-md-0">

        <div class="col-sm-5 col-md-8 pt-5">

          <span class="number-span pt-2">01.</span>

          <p class="paragraph-title pt-3">Mapeamento</p>

          <p class="pt-3 paragraph-map">Nossa metodologia consiste em analisar a situação das empresas, por meio de dados fornecidos e entrevistas com os executivos, realizando assim um diagnóstico do cenário econômico-financeiro.</p>

        </div>

        <div class="bg-box">

          <img class="img-fluid rounded m-auto pt-5 d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mapeamento.png" alt="">

        </div>

      </div>


      <div class="col-md-6 position-relative py-4 py-md-0">

        <div class="col-sm-5 col-md-8 pt-5">

          <span class="number-span pt-2">02.</span>

          <p class="paragraph-title pt-3">Planejamento</p>

          <p class="pt-3 paragraph-map">A partir disso, definimos os planos de ações em um cronograma pré-estabelecido para cada cenário, tomando-se os devidos cuidados para que produzam os efeitos necessários na implantação.</p>

        </div>

        <div class="bg-box">

          <img class="img-fluid rounded m-auto pt-5 d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/planejamento.png" alt="">

        </div>

      </div>

      <div class="col-md-6"></div>

      <div class="col-md-6"></div>

      <div class="col-md-6 position-relative py-4 py-md-0">

        <div class="col-sm-5 col-md-8 pt-5">

          <span class="number-span pt-2">03.</span>

          <p class="paragraph-title pt-3">Gerenciamento</p>

          <p class="pt-3 paragraph-map">Quando necessário, disponibilizamos uma equipe de consultores para atuar de forma conjunta aos executivos nas tarefas de administração da empresa para um melhor gerenciamento do projeto.</p>

        </div>

        <div class="bg-box">

          <img class="img-fluid rounded m-auto pt-5 d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/gerenciamento.png" alt="">

        </div>

      </div>


      <div class="col-md-6 position-relative py-4 py-md-0">

        <div class="col-sm-5 col-md-8 pt-5">

          <span class="number-span pt-2">04.</span>

          <p class="paragraph-title pt-3">Capacitação</p>

          <p class="pt-3 paragraph-map">Ao final dos nossos trabalhos, o objetivo é que os profissionais diretamente envolvidos no processo tenham absorvido a metodologia de gestão para dar continuidade no sucesso da empresa.</p>

        </div>

        <div class="bg-box">

          <img class="img-fluid rounded m-auto pt-5 d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/capacitacao.png" alt="">

        </div>

      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>