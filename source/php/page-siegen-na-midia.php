<?php get_header(); ?>
<div class="videos">
  <div class="bg-dgrey d-flex align-items-center slide">
    <div class="container">
      <h1 class="text-white text-center text-md-left">Siegen na Mídia</h1>
    </div>
  </div>
  <div class="px-4">
    <div class="owl-videos owl-carousel owl-theme">
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/6MSMBiSbm1M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/quWQ3CZ3JYU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/JEzGy-sxHpw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/tWJFs3wo07I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/5ET9LyhwKkA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/xzxBQZlmDpo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/ZVL9r4y7gT0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/b5ZcICM66ts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
    </div>

    <p class="mb-3 text-center">Para mais vídeos, visite e se inscreva em nosso canal do YouTube</p>

    <div class="text-center">
      <div class="nav-d owl-nav"></div>
      <div class="d-md-none">
        <div class="my-4 owl-dots" id="navDots"></div>
      </div>
      <div class="nav-m owl-nav"></div>
    </div>
  </div>
  <div class="container mt-5 my-5">
    <div class="text-center my-md-4">
      <h2 class="color-green my-3">Matérias</h2>
      <p>Clique nos links abaixo para ler as matérias que realizamos:</p>
    </div>
    <div class="owl-materias owl-carousel owl-theme mt-3">
      <div class="item">
        <a class="hover-bt" href="https://diariodocomercio.com.br/opiniao/apesar-da-situacao-ha-luz-no-tunel-para-a-vale/" target="_blank">
          <div class="box"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Vale-2-1068x670.jpg"></div>
          <h4 class="color-green mt-4">Apesar da situação, há luz no túnel para a vale</h4>
        </a>
      </div>
      <div class="item">
        <a class="hover-bt" href="http://diariodocomercio.com.br/opiniao/crescimento-irrisorio-do-pib/" target="_blank">
          <div class="box"><img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/pib-1238368-pxhere.com_-696x410.jpg"></div>
          <h4 class="color-green mt-4">Crescimento irrisório do PIB</h4>
        </a>
      </div>
      <div class="item">
        <a class="hover-bt" href="https://cbn.globoradio.globo.com/media/audio/265809/falta-de-preparo-contribui-para-fechamento-de-empr.htm" target="_blank">
          <div class="box"><img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/crop_detail.jpg"></div>
          <h4 class="color-green mt-4">Falta de preparo contribui para fechamento de empresas em momentos de crise econômica</h4>
        </a>
      </div>
      <div class="item"><iframe width="560" height="315" src="https://www.youtube.com/embed/JEzGy-sxHpw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4 class="color-green mt-4">Fábio Astrauskas comenta sobre o impacto da reforma da previdência no aquecimento da economia</h4>
      </div>
      <div class="item">
        <a class="hover-bt" href="https://epoca.globo.com/coluna-novela-avianca-brasil-esta-longe-muito-longe-do-fim-23943167" target="_blank">
          <div class="box"><img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/xAviancaDivulgacao.jpg.pagespeed.ic.b6R-X72Ma6.jpg"></div>
          <h4 class="color-green mt-4">Coluna | Novela 'Avianca Brasil' está longe, muito longe do fim</h4>
        </a>
      </div>
      <div class="item">
        <a class="hover-bt" href="http://cbn.globoradio.globo.com/media/audio/278241/unica-solucao-para-pedidos-de-falencia-e-que-econo.htm" target="_blank">
          <div class="box"><img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/aa35f16c-590e-4f1a-89ba-a278360cc75d.jpg.640x360_q75_box-0,100,1920,1180_crop_detail.jpg"></div>
          <h4 class="color-green mt-4">Única solução para pedidos de falência é que economia retome as atividades</h4>
        </a>
      </div>
    </div>
    <div class="row justify-content-center align-items-center">
      <div id="materiaNav" class="owl-nav col-md-6 text-center"></div>
    </div>
  </div>
</div>
<div class="bg-dgrey py-5 my-md-4">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-7 text-white mb-4 mb-md-0">
        <h3>Interessou-se?</h3>
        <p>Se você se identificou pela nossa missão e com os valores que propagamos, entre em contato.</p>
      </div>
      <div class="col-md-5"><a href="<?= get_site_url() ?>/contato" class="bg-green px-5 py-3 text-white hover-bt">Entrar em contato</a></div>
    </div>
  </div>
</div>
<?php get_footer(); ?>