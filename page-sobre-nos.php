<?php get_header(); ?> <div class="bg-dgrey full-center bg-sobrenos position-relative mb-md-5"><!-- <h1 class="text-white" id="animated-txt"></h1> --> <svg height="100" id="animation" stroke="#fff" stroke-width="2" class="text-line d-md-none animation" width="100%"><text x="50%" dominant-baseline="middle" text-anchor="middle" y="50%">Siegen</text></svg><h1 id="hoversiegen" class="text-line d-lg-flex d-none text-white box-animation align-items-center justify-content-center my-5"><div class="item mt-5"><span class="text-uppercase">S</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Serviços de</h3></div></div></div><div class="item infomacoes mt-5"><span>i</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Informação</h3></div></div></div><div class="item empresarial mt-5"><span>e</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Empresarial e</h3></div></div></div><div class="item gestao mt-5"><span>g</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Gestão</h3></div></div></div><div class="item estrategia mt-5"><span>e</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Estratégica de</h3></div></div></div><div class="item negocios mt-5"><span>n</span><div class="d-nonee position-absolute show"><div class="d-flex align-items-center box"><div class="detail"></div><h3 class="mb-0">Negócios</h3></div></div></div></h1><div class="position-absolute siegen-sig">Palavra do idioma alemão: sie.gen ['zi:gen] - Tradução de "Vencer", "Triunfar". Pronuncia-se "Zíguen".</div></div><div class="container my-5 text-center text-md-left sobre-nos"><div class="row"><div class="col-md-5"><h2 class="color-green mb-4">Sobre nós</h2><p>Fundada em 1996 por Augusto Paes Barreto e Fábio Astrauskas, a Siegen tornou-se rapidamente uma das mais <b>conceituadas</b> empresas de consultoria em gestão estratégica e recuperação de empresas do Brasil.</p><img class="d-none d-md-block my-3 pb-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobre-nos-foto.png" alt="Sala de reunião Siegen" title="Sala de reunião Siegen"><p>Hoje nosso escritório conta com mais de 80 colaboradores altamente capacitados, especializados em áreas multidisciplinares, que atuam na busca das melhores soluções para sua empresa.</p></div><div class="col-md-7"><div class="owl-sobre owl-carousel owl-theme mb-md-4"><div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/IMG_0004.jpg" alt="Sala de espera Siegen"></div><div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/MaskGroup.png" alt="Sala de espera Siegen"></div><div class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/IMG_0042.jpg" alt="Sala de espera Siegen"></div></div><!-- <div class="col-12 my-md-3 d-flex justify-content-center">
                <a href="sobre-nos" class="bg-green px-5 py-3 hover-bt text-white">Saiba mais</a>
            </div> --></div></div></div><div class="container mt-md-n5"><p class="text-md-left text-center">Os mais de 300 projetos desenvolvidos em <b>nossos mais de 20 anos de história</b>, nos trouxeram a credibilidade e o reconhecimento de instituições financeiras como, FIDCs, factorings e bancos tradicionais, que, atuando em parceria conosco, oferecem a <b>captação de linhas de crédito a curto prazo para os nossos clientes</b>. Todo esse know-how também permitiu a diversificação dos negócios. Ampliamos o nosso portfólio de serviços e hoje também atuamos na consultoria das áreas de marketing, vendas e produção.</p><p class="text-md-left text-center">A Siegen é a empresa fundadora da <b>TMA Brasil</b>, representante brasileira da Turnaround Management Association, a mais prestigiosa associação mundial que reúne profissionais envolvidos com recuperação da performance e do valor das empresas e organizações em geral. Além disso, a Siegen também faz parte da <b>Amcham Brasil</b> (Câmara Americana).</p></div><!-- 
<div class="bg-motiva mt-5">

    <div class="container text-center text-white h-100">

        <div class="full-center h-100">

            <div class="box">

                <h2 class="font-weight-bold mb-md-4">Motivação Siegen</h2>

                <p class="mt-4 mb-4 text-justify">

                    Nosso país passou por diversos períodos onde a alta dos juros e as sucessivas crises cambiais criaram um cenário extremamente hostil para o empresário brasileiro. Por isso, profissionalizar a gestão financeira e pensar estrategicamente é fundamental para sobreviver em um mercado com tantas adversidades.
                </p>

                <p class="text-justify">
                    O que a gente melhor sabe fazer é desenvolver soluções para cada problema e atuar, de maneira preventiva ou corretiva, sempre dentro da ética e de acordo com as leis vigentes, para que nossos serviços façam a vida financeira dos clientes cada vez mais sadia.
                </p>

            </div>

        </div>

    </div>

</div> --><div class="bg-dgrey py-5 mt-5"><div class="container"><div class="row text-white"><div class="col-md-12 m-auto"><h2 class="mb-md-5 text-center mb-4">Missão</h2><p class="pl-3 pl-md-0">A missão da Siegen é aumentar a capacidade de geração de caixa de seus clientes, por meio da reestruturação financeira, comercial, operacional e administrativa, utilizando técnicas e instrumentos de consultoria em gestão empresarial.</p><p class="pl-3 pl-md-0">Os resultados alcançados deverão promover o crescimento sustentável de nossa organização, bem como desenvolver e manter colaboradores de alto desempenho profissional.</p>Palavras-Chave:<ul><li>Desenvolver soluções: propor e executar</li><li>Aperfeiçoar: processo de melhoria contínua</li><li>Administração estratégica: planejar, organizar, dirigir e controlar</li><li>Técnicas e instrumentos de gestão e consultoria: nossos produtos</li><li>Alto desempenho: novos valores</li></ul><p></p><h2 class="text-center mb-4">Valores</h2><div class="d-flex flex-wrap mt-md-5"><div class="col-md-6 pl-md-0"><p><b>• Realização:</b> temos como meta central o sucesso, nosso e dos nossos clientes, portanto, a busca pelo desempenho realizado sempre com alta performance é o que norteia nosso dia a dia.</p><p><b>• Conformidade:</b> temos a clara noção de limites das ações organizacionais, por isso damos prioridade ao respeito às regras e aos comportamentos no ambiente de trabalho e no relacionamento com as organizações.</p><p><b>• Bem-estar: </b>preocupação constante da Siegen em proporcionar satisfação ao colaborador, atentando-se para a qualidade de vida.</p></div><div class="col-md-6 pl-md-0"><p><b>• Prestígio Organizacional:</b> a Siegen e seus colaboradores buscam o prestígio, a admiração e o respeito da sociedade, mediante a qualidade dos seus serviços prestados e da credibilidade das suas ações.</p><p><b>• Autonomia:</b> vivemos uma busca de aperfeiçoamento constante dos colaboradores e de toda a empresa. Isso se expressa por meio da curiosidade, da criatividade, das novas experiências, da definição de objetivos profissionais e da nossa abertura para novos desafios.</p></div></div></div></div></div></div><!-- <div class="py-5 my-md-4 d-none d-md-block"> --><!-- <div class="align-items-center justify-content-center text-center">

        <h2 class="color-green col-12 text-center mb-2">Linha do Tempo</h2>

        <img class="mt-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/linha-do-tempo.png" alt="">

        <img class="gif-ajust" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/linha-do-tempo.gif">

    </div> --><!-- <div class="col-md-2 d-flex text-center justify-content-center">

            <ul class="nav d-md-block justify-content-center ali" id="myTab" role="tablist">

                <li class="nav-item">
                    <a class="active" id="1995-date" data-toggle="tab" href="#date" role="tab" aria-controls="1995" aria-selected="true">
                        1995
                    </a>
                </li>

                <li class="nav-item">
                    <a class=" " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        1996
                    </a>
                </li>
                <li class="nav-item">
                    <a class="" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                        2001</a>
                </li>
                <li class="nav-item">
                    <a class="" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                        2005</a>
                </li>

                <li class="nav-item">
                    <a class="" id="dated-2016" data-toggle="tab" href="#date-2016" role="tab" aria-controls="contact" aria-selected="false">
                        2016</a>
                </li>
            </ul>

        </div> 

        <div class="col-md-10">

            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="date" role="tabpanel" aria-labelledby="1995-date">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">

                            <p class="mt-4">

                                Primeiros trabalhos realizados de Fabio e Augusto como consultoria de reestruturação e recuperação de empresas

                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">


                            <p class="mt-4">

                                Fundação da Siegen atuando já em grandes clientes
                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">


                            <p class="mt-4">

                                Início dos projetos de consultoria comercial, produção e administrativo
                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>
                </div>

                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">


                            <p class="mt-4">

                                Início dos primeiros trabalhos de plano de recuperação judicial, sendo pioneira no Brasil neste segmento
                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">


                            <p class="mt-4">

                                Início dos primeiros trabalhos de plano de recuperação judicial, sendo pioneira no Brasil neste segmento
                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="date-2016" role="tabpanel" aria-labelledby="dated-2016">

                    <div class=" d-flex flex-wrap align-items-center">

                        <div class="col-md-6">


                            <p class="mt-4">

                                Mudança do escritório para Alto de Pinheiros
                            </p>

                        </div>

                        <div class="col-md-6">

                            <div class="bg-grey box-img"></div>

                        </div>

                    </div>

                </div>


            </div>

        </div> --><!-- </div> --> <?php include('nossos-numeros.php') ?> <?php include('equipe.php') ?> <?php get_footer(); ?>