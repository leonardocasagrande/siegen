<?php get_header(); ?> <div class="cases-sucesso"><div class="bg-clientes slide"><div class="container h-100"><div class="d-flex h-100 align-items-end box-content"><div class="box text-white"><h1 class="mb-3">Cases de sucesso</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra sapien lectus, nec lobortis lorem efficitur a. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p></div></div></div></div><div class="container"><div class="d-flex flex-wrap"> <?php

      $paged = get_query_var('paged') ? get_query_var('paged') : 1;

      $args = array(

        'post_type' => 'post',

        'order' => 'ASC',

        'posts_per_page' => '6',

        'paged' => $paged,

      );

      $loop = new wp_query($args);

      while ($loop->have_posts()) : $loop->the_post() ?> <div class="item d-flex flex-wrap align-items-center"><div class="box-item bg-lgrey h-100 img-post d-nonee"></div><div class="bg-dgrey box-item h-100 full-center flex-wrap"><div class="col-12"><p class="text-white">Author name</p><h2 class="color-green"><?= the_title(); ?></h2></div><div class="col-12"><a href="<?= get_the_permalink(); ?>" class="text-white hover-bt">Leia mais</a></div></div></div> <?php endwhile; ?> </div></div></div> <?php get_footer(); ?>