<!DOCTYPE html><html lang="pt_BR"><head><meta name="adopt-website-id" content="dfb8790a-cef9-4e38-8cd7-892d1123ccef"><script src="//tag.goadopt.io/injector.js?website_code=dfb8790a-cef9-4e38-8cd7-892d1123ccef" class="adopt-injector"></script><!-- Google Tag Manager --><script>(function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-57H7GP2');</script><!-- End Google Tag Manager --><meta charset="UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title><?php wp_title(''); ?></title><meta name="robots" content="index, follow"><meta name="msapplication-TileColor" content="#ffffff"><meta name="theme-color" content="#ffffff"> <?php wp_head(); ?> </head><body><!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57H7GP2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><!-- End Google Tag Manager (noscript) --><!-- <noscript id="deferred-styles"></noscript> --><link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css"><!-- <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style-new.css" /> --> <?php if (is_single()) {

        $greyscale = 'grayscale';

        $colorGrey = 'color-dgrey';

        $bgGreen = 'bg-green';
    } ?> <header class="py-md-4 py-2 before-class"><nav class="d-flex justify-content-between flex-wrap navbar-expand-lg after-class"><a class="navbar-brand pl-4" href="<?= get_home_url(); ?>"><img class="<?= $greyscale ?>" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-white1.png"> </a><button class="navbar-toggler pr-5" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars text-white"></i></button><div class="collapse navbar-collapse py-3" id="navbarSupportedContent"><ul class="navbar-nav ml-auto align-items-center"><li class="nav-item active"><a class="nav-link <?= $colorGrey ?>" href="sobre-nos/">Sobre nós</a></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" href="equipe/">Equipe</a></li><!-- <li class="nav-item">
            <a class="nav-link <?= $colorGrey ?>" href="cases-de-sucesso/">Cases</a>
          </li> --><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" href="servicos/">Serviços</a></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" href="siegen-na-midia/">Siegen na Mídia</a></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" href="contato">Seja nosso cliente</a></li><li class="nav-item dropdown"><a class="nav-link <?= $colorGrey ?> dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">Canal de Ética</a><div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="m-0 dropdown-item" target="_blank" href="https://ouvidordigital.com.br/siegenouvidoria/">Ouvidoria</a> <a class="m-0 dropdown-item" target="_blank" href="https://ouvidordigital.com.br/siegen/">Denúncia</a> <a class="m-0 dropdown-item" target="_blank" href="https://siegen.com.br/manual/Manual-de-Conduta-Etica-e-Empresarial-26062020.pdf">Código de Conduta</a></div></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" target="_blank" href="https://www.linkedin.com/company/siegen-ltda/"><i class="fab text-white fa-linkedin-in"></i></a></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" target="_blank" href="https://www.instagram.com/siegenconsultoriafinanceira/"><i class="fab text-white fa-instagram"></i></a></li><li class="nav-item"><a class="nav-link <?= $colorGrey ?>" target="_blank" href="https://www.youtube.com/siegenconsultoriafinanceira"><i class="fab text-white fa-youtube"></i></a></li></ul></div><!-- <li class="nav-item">

            <a class="nav-link <?= $colorGrey ?>" target="_blank" href="">

              Espaço Colaborar/Cliente

            </a>

          </li> --></nav><div class="w-100 text-white container mt-2 d-none d-lg-block"><div class="row mt-n4"><div class="col-md-4"></div><div class="col-md-8 d-flex justify-content-center"><div class="d-flex">Consultoria especializada em<div class="font-weight-bold ml-1">reestruturação e recuperação de empresas</div></div></div></div></div></header></body><div class="wrapper-whats"><a href="https://ouvidordigital.com.br/siegenouvidoria/" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-ouvidor-digital.png" alt="ouvidor digital"> </a><a href="https://api.whatsapp.com/send?phone=5511992507017" target="_blank" class="btn-whats"><i class="text-white fab fa-whatsapp"></i></a></div><style>.wrapper-whats {
        display: flex;
        align-items: center;
        justify-content: center;
        position: fixed;
        bottom: 40px;
        right: 40px;
        z-index: 100;

    }

    @media(max-width: 400px) {
        .wrapper-whats {
            flex-direction: column;
            bottom: 25px !important;
            right: 25px !important;
        }

        .wrapper-whats img {
            margin-bottom: 10px;
            width: 70px !important;
        }

        .wrapper-whats .btn-whats {
            margin-left: 0 !important;
        }
    }

    .wrapper-whats img {
        width: 100px;

    }

    .btn-whats {
        height: 60px;
        width: 60px;
        background-color: #00AA4E;
        border-radius: 50% !important;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 28px !important;
        margin-left: 20px;
    }

    .btn-whats:hover {
        background-color: #058a43;

    }</style></html>